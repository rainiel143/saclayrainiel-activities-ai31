//tablink
function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("content");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();



google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(columnChart);
      google.charts.setOnLoadCallback(bookChart);
      

//column
      function columnChart() {

        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Ambot');
        data.addColumn('number', 'Book');
        data.addRows([
          ['Total', 656413],
          ['Borrowed', 258957],
          ['Damage', 89895],
          ['Missing', 18950]
        ]);

        // Set options for Anthony's pie chart.
        var options = {title:'BOOKS',
          chartArea:{
              left:50
            }

        };

        // Instantiate and draw the chart for Anthony's pizza.
        var chart = new google.visualization.ColumnChart(document.getElementById('columnchart'));
        chart.draw(data, options);
      }


//pie chart


      function bookChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'CATEGORY'],
          ['Fantasy',     545],
          ['Adventure',      780],
          ['Romance',  609],
          ['Contemporary', 450],
          ['Horror', 690],
          ['Thriller', 895],
          ['Mystery',    569]
        ]);

        var options = {
              title: 'BOOK CATEGORY',
              chartArea:{
              left:10,
              right:10,
              bottom:20,
              top:40,
              width:"50%",
              height:"100%"
          }
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
        
      }