import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import VueRouter from "vue-router";
import './plugins/bootstrap-vue'
import App from './App.vue'
import Dashboard from './components/Dashboard.vue'
import Patron from './components/Patron.vue'
import Book from './components/Book.vue'
import Settings from './components/Settings.vue'

Vue.use(VueRouter);

const router = new VueRouter({
  routes:  [
    {
      path: "/",
      component: Dashboard
    },
    {
      path: "/patron",
      component: Patron
    },
    {
      path: "/book",
      component: Book
    },
    {
      path: "/settings",
      component: Settings
    },
  ]
});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
